import './App.css';
import React from 'react';

class App extends React.Component {
constructor(props){
  super(props)
  this.date = new Date();
  this.state={
    days:'',
    hours:'',
    dateCompletes:'',
    hBtn: false,
    mBtn: false, 
    jBtn: false,
  }

}

componentDidMount= async()=>{
  this.date = new Date();
  this.setState({
    days:'',
  })
  this.setState({
    hours:'',
  })
  this.setState({
    dateCompletes:'',
  })
   
}
componentWillUnmount(){
}
//fonction qui change l'état du bouton de l'heure 
 addHour = ()=>{
  this.setState({
    hBtn: true
  });
 }
//fonction qui change l'état du bouton du jour  
 addDay = ()=>{
  this.setState({
    dBtn: true
  })
 }
//fonction qui structure le composant
 appInterval(){
  this.date = new Date();
  var day = this.date.getDay();
  var jour = this.date.getDate(); //jour
  var moment = '';

  if(jour<10){jour="0"+jour};
  var mois = this.date.getMonth(); //mois
  var annee = this.date.getFullYear(); // année
  var heure = this.date.getHours(); //heure
  if(heure<10){heure="0"+heure};
  var minutes = this.date.getMinutes(); //minutes
  if(minutes<10){minutes="0"+minutes};
  var secondes = this.date.getSeconds(); //secondes
  if(secondes<10){secondes="0"+secondes};
  
  if(heure<12){moment = 'Matin'};
  if(heure===12 && minutes<59){moment = 'Midi'};
  if(heure>=13){moment = 'Apres-midi'};
  if(heure>15){moment = 'soir'};
  if(heure>=19){moment = 'Nuit'};
  const tabJour = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche'];
  const tabMois = ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre']
  var newHour = this.date.getHours() + 1;
  var newJour = this.date.getDate() + 1;
  var newDay =  this.date.getDay() + 1;
  var hBtn = this.state.hBtn;
  var dBtn = this.state.dBtn;
  //affichage conditionnel du jour
  if(dBtn){
    this.setState({
      dateCompletes:
      <h1><br/>{""+newJour+" "+tabMois[mois].toUpperCase()+" "+annee}</h1>
    });
    if(newDay<=30){
      this.setState({
        days:
        <h1><span className='Jour'>{tabJour[newDay-1].toUpperCase()}</span>
        <br/>{moment.toUpperCase()}</h1>
      });
    }
    else{
      newDay = newDay - 7 ;
    this.setState({
      days:
      <h1><span className='Jour'>{tabJour[newDay-1].toUpperCase()}</span>
      <br/>{moment.toUpperCase()}</h1>
    });
  }
  }
  else{
    this.setState({
      dateCompletes:
      <h1><br/>{""+jour+" "+tabMois[mois].toUpperCase()+" "+annee}</h1>
    });
    this.setState({
      days:
      <h1><span className='Jour'>{tabJour[day-1].toUpperCase()}</span>
      <br/>{moment.toUpperCase()}</h1>
    });
  }
  //affichage conditionnel de l'heure
  if(hBtn){
    this.setState({
      hours:
      <h1><br/><span className="Heure">{""+newHour+":"+minutes+":"+secondes}</span></h1>
    });
  }
  else{
    this.setState({
      hours:
      <h1><br/><span className="Heure">{""+heure +":"+minutes+":"+secondes}</span></h1>
    });
  }
  return (
    <div className='App'>
      <div className='Clock'> {this.state.days} {this.state.hours} {this.state.dateCompletes} </div>
      <div className='ClockBtn'>
        <button onClick={() => this.addDay()}>J</button>
        <button onClick={() => this.addHour()}>H</button>
        <button onClick={() => this.addMinute()}>M</button>
      </div>
    </div>
    ); 
 }

  render() {
    setInterval(()=>this.appInterval(), 1000);   
    return (
      <div className='App'>
        <div className='Clock'> {this.state.days} {this.state.hours} {this.state.dateCompletes} </div>
        <div className='ClockBtn'>
          <button onClick={() => this.addDay()}>J</button>
          <button onClick={() => this.addHour()}>H</button>
          <button onClick={() => this.addMinute()}>M</button>
        </div>
      </div>
      ); 
  }
}
export default(App);